#!/bin/python
from subprocess import call
import os
import sys
import shutil

def switch_dir():
	abspath = os.path.abspath(__file__)
	dname = os.path.dirname(abspath)
	os.chdir(dname)

def build():
	print "Build started!"
	call(["mkdir -p ./Linux_Build"], shell=True)
	call(["mcs ./Outburner/*.cs -out:./Linux_Build/outburner_linux.exe"], shell=True)
	print "------------------------"

def run():
	print "Run started!"
	call(["mono ./Linux_Build/outburner_linux.exe"], shell=True)
	print "------------------------"

def clean():
	print "Clean started!"
	shutil.rmtree("./Linux_Build", ignore_errors=True)
	print "------------------------"

def help():
	print "Mono based builder for outburner project"
	print "Please use one or more switches"
	print "    -clean: cleans the Linux_Build directory"
	print "    -build: builds outburner into the Linux_Build directory"
	print "    -run: start the Linux_Build/outburner.exe with mono"
	print "    -h: show this help"

def is_arg_exist(arg):
	for a in sys.argv[1:]:
		if arg == a:
			return True
	return False

if len(sys.argv) == 1:
	help()
else:
	switch_dir()
	if is_arg_exist("-h"):
		help()
	if is_arg_exist("-clean"):
		clean()
	if is_arg_exist("-build"):
		build()
	if is_arg_exist("-run"):
		run()
	print "Ready"

