﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Outburner
{
    class Program
    {
        public const string CONF_OUTPUT_FOLDER = "outputfolder";
        public const string CONF_INPUT_FOLDER = "inputfolder";
        public const string CONF_AUTO_CLOSE = "autoclose";

        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            GetArgs(new CommandArgumentParser(args));
            CheckForConfig();
            CleanOutput();
            //CopyStaticFiles();
            Log.WriteLine("Check layout file");
            Layout layout = new Layout();
            Log.WriteLine("Initialize filewalker");
            FileWalker fileWalker = new FileWalker(layout, new string[] 
            {
                layout.filePath,
                GetAbsoluteInputPath("_Outburner/log.txt"),
                GetAbsoluteInputPath("_Outburner/outburner.exe"),
                GetAbsoluteInputPath("_Outburner/outburner.cfg")
            });
            fileWalker.Run();
            double milis = (DateTime.Now - start).TotalMilliseconds;
            Log.WriteLine("Filewalker ended in "+Math.Round(milis,2)+"ms.");
            if (!IsAutoClose())
            {
                Console.WriteLine("Press a key to close");
                Console.ReadKey();
            }
        }

        static void GetArgs(CommandArgumentParser parser) {
            Log.WriteLine("Parsing command arguments");
            Config.DEFAULT_CONFIG_FILE = parser.GetString("-c", Config.DEFAULT_CONFIG_FILE);
            Log.WriteLine("Using config: "+Config.DEFAULT_CONFIG_FILE);
        }

        static void CheckForConfig()
        {
            try
            {
                Log.WriteLine("Checking for config file");
                Config cfg = Config.GetInstance();
                //Output folder
                Log.WriteLine("Checking for output folder");
                string outputFolder = cfg.GetValue(CONF_OUTPUT_FOLDER);
                if (!Directory.Exists(outputFolder))
                    ExitWithError("Output folder not exist!");

                //InputFolder
                Log.WriteLine("Checking for input folder");
                string inputFolder = cfg.GetValue(CONF_INPUT_FOLDER);
                if (!Directory.Exists(inputFolder))
                    ExitWithError("Input folder not exist!");
            }
            catch (FileNotFoundException)
            {
                ExitWithError("Config file not found!");
            }
            catch (KeyNotFoundException exc)
            {
                ExitWithError(String.Format("Config key {0} not found in config file", exc.Message));
            }
            catch (Exception)
            {
                ExitWithError("Unkown error while loading config!");
            }
        }

        public static void ExitWithError(string errorMessage)
        {
            Log.WriteLine("FATAL ERROR: "+errorMessage);
            Console.ReadKey();
            Environment.Exit(0);
        }

        static void CleanOutput()
        {
            Log.WriteLine("Clean output folder");
            string outputDir = Config.GetInstance().GetValue(CONF_OUTPUT_FOLDER);
            Directory.Delete(outputDir, true);
            Directory.CreateDirectory(outputDir);
        }

        static void CopyStaticFiles()
        {
            Log.WriteLine("Copy static folder");
            string inputDir = Config.GetInstance().GetValue(CONF_INPUT_FOLDER) + "/_Content";
            string outputDir = Config.GetInstance().GetValue(CONF_OUTPUT_FOLDER) + "/_Content";
            if (!Directory.Exists(inputDir)) return;
            Directory.CreateDirectory(outputDir);
            DirectoryCopy(inputDir, outputDir, true);
        }

        static bool IsAutoClose()
        {
            try
            {
                bool autoClose = bool.Parse(Config.GetInstance().GetValue(CONF_AUTO_CLOSE));
                return autoClose;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static string GetAbsoluteInputPath(string relativePath)
        {
            return Config.GetInstance().GetValue(CONF_INPUT_FOLDER) + "//" + relativePath;
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}
