﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Outburner
{
    class Layout
    {
        const string LAYOUT_FILE_NAME = "layout.php";
        const string PAGE_CONTENT_TOKEN = "{{PAGE:CONTENT}}";
        public string filePath { get; protected set; }
        string layoutFileContent;

        public Layout()
        {
            filePath = Config.GetInstance().GetValue(Program.CONF_INPUT_FOLDER) + "/" + LAYOUT_FILE_NAME;
            if (!File.Exists(filePath)) Program.ExitWithError(filePath + " file not exists!");
            Read();
            if (!layoutFileContent.Contains(PAGE_CONTENT_TOKEN))
                Program.ExitWithError(String.Format("The layout file don't have the \"{0}\" page content token", PAGE_CONTENT_TOKEN));
        }

        void Read()
        {
            StreamReader r = new StreamReader(filePath);
            layoutFileContent = r.ReadToEnd();
            r.Close();
        }

        public string GetContent()
        {
            return layoutFileContent;
        }

        public string RenderPageContent(string pageContent)
        {
            return layoutFileContent.Replace(PAGE_CONTENT_TOKEN, pageContent);
        }
    }
}
