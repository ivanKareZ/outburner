﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Outburner
{
    class Config
    {
        public static string DEFAULT_CONFIG_FILE = "outburner.cfg";

        Dictionary<string, string> configParams;
        string configFilePath;

        public Config(string filePath)
        {
            configFilePath = filePath;
            LoadConfig();
        }

        void LoadConfig()
        {
            configParams = new Dictionary<string, string>();
            StreamReader r = new StreamReader(configFilePath);
            while (!r.EndOfStream)
            {
                string line = r.ReadLine();
                if (line.StartsWith("#")) continue;
                if (!line.Contains(":")) continue;
                string[] splitted = SplitLine(line);
                string key = splitted[0];
                string value = splitted[1];
                configParams.Add(key, value);
            }
            r.Close();
        }

        string[] SplitLine(string line)
        {
            string key = "";
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == ':')
                    break;
                key += line[i];
            }
            string value = line.Remove(0, key.Length+1);
            return new string[] { key, value };
        }

        public string GetValue(string key)
        {
            if (!configParams.ContainsKey(key))
                throw new KeyNotFoundException(key);
            return configParams[key];
        }

        static Config instance;
        public static Config GetInstance()
        {
            if (instance == null)
                instance = new Config(DEFAULT_CONFIG_FILE);
            return instance;
        }

        public void AddOrRenew(string key, string value)
        {
            if (configParams.ContainsKey(key))
            {
                Log.WriteLine("Renew variable " + key + " to " + value + " from " + configParams[key]);
                configParams[key] = value;
            }
            else
            {
                Log.WriteLine("Add variable " + key + " = " + value);
                configParams.Add(key, value);
            }
        }

        public Dictionary<string, string> GetDictionary()
        {
            return new Dictionary<string, string>(configParams);
        }
    }
}
