﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Outburner
{
    class Log
    {
        public static void Init()
        {
            if (File.Exists("log.txt"))
                File.Create("log.txt");
        }

        public static void WriteLine(string message)
        {
            Console.WriteLine(message);
            string logLine = String.Format("{0}:: {1}\n", DateTime.Now, message);
            File.AppendAllText("log.txt", logLine);
        }
    }
}
