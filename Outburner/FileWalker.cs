﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Outburner
{
    class FileWalker
    {
        const string IGNORE_TAG = "ignore layout";
        string[] exceptions;
        Layout layout;

        public FileWalker(Layout layout, string[] exceptions)
        {
            this.exceptions = exceptions;
            this.layout = layout;
        }

        public void Run()
        {
            Log.WriteLine("Start filewalker");
            Config cfg = Config.GetInstance();
            string inputDirPath = cfg.GetValue(Program.CONF_INPUT_FOLDER);
            ProcessDirectory(inputDirPath);
        }

        void ProcessDirectory(string path)
        {
            Log.WriteLine("Processing directory: "+path);
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                ProcessFile(file);
            }

            string[] directories = Directory.GetDirectories(path);
            foreach (string directory in directories)
            {
                ProcessDirectory(directory);
            }  
        }

        void ProcessFile(string filePath)
        {
            try
            {
                Log.WriteLine("Processing file: " + filePath);
                if (Path.GetExtension(filePath) != ".php"
                    && Path.GetExtension(filePath) != ".html"
                    && Path.GetExtension(filePath) != ".css"
                    && Path.GetExtension(filePath) != ".js")
                {
                    ExportCopy(filePath);
                    return;
                }
                if (isException(filePath)) return;
                string fileContent = "";
                StreamReader r = new StreamReader(filePath);
                bool ignoreLayout = false;
                while (!r.EndOfStream)
                {
                    string line = r.ReadLine();
                    if (line.StartsWith("#"))
                        if (line.Contains(IGNORE_TAG))
                            ignoreLayout = true;
                        else
                            ParseVariable(line);
                    else
                        fileContent += line + "\n";
                }
                r.Close();
                string extendedFileContent = fileContent;
                if (!ignoreLayout)
                {
                    Log.WriteLine("Rendering layout");
                    extendedFileContent = layout.RenderPageContent(fileContent);
                }
                Log.WriteLine("Replacing tokens");
                string finalPage = ReplaceTokens(extendedFileContent);
                Log.WriteLine("Exporting final file");
                Export(filePath, finalPage);
                GC.Collect();
            } catch (Exception exc)
            {
                Log.WriteLine("Processing file failed because: " + exc.Message);
            }
        }

        void Export(string originalFilePath, string renderedContent)
        {
            try
            {
                string inputPath = Path.GetFullPath(Config.GetInstance().GetValue(Program.CONF_INPUT_FOLDER));
                string fileDirectory = Path.GetFullPath(Path.GetDirectoryName(originalFilePath));
                string diff = fileDirectory.Remove(0, inputPath.Length);
                string outputPath = Path.GetFullPath(Config.GetInstance().GetValue(Program.CONF_OUTPUT_FOLDER));
                string fileOutputDir = outputPath + diff;
                Directory.CreateDirectory(fileOutputDir);
                StreamWriter writer = new StreamWriter(fileOutputDir + "/" + Path.GetFileName(originalFilePath));
                writer.Write(renderedContent);
                writer.Close();
            } catch (Exception exc)
            {
                Log.WriteLine("Exporting file failed because: " + exc.Message);
            }
        }

        void ExportCopy(string originalFile)
        {
            try
            {
                string inputPath = Path.GetFullPath(Config.GetInstance().GetValue(Program.CONF_INPUT_FOLDER));
                string fileDirectory = Path.GetFullPath(Path.GetDirectoryName(originalFile));
                string diff = fileDirectory.Remove(0, inputPath.Length);
                string outputPath = Path.GetFullPath(Config.GetInstance().GetValue(Program.CONF_OUTPUT_FOLDER));
                string fileOutputDir = outputPath + diff;
                Directory.CreateDirectory(fileOutputDir);
                string outFile = fileOutputDir + "/" + Path.GetFileName(originalFile);
                File.Copy(originalFile, outFile);
            }
            catch (Exception exc)
            {
                Log.WriteLine("Exporting file failed because: " + exc.Message);
            }
        }

        void ParseVariable(string line)
        {
            if (!line.Contains(":")) return;
            string[] kv = SplitLine(line);
            Config.GetInstance().AddOrRenew(kv[0], kv[1]);
        }

        string[] SplitLine(string line)
        {
            line = line.Remove(0,1);
            string key = "";
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == ':')
                    break;
                key += line[i];
            }
            string value = line.Remove(0, key.Length + 1);
            Log.WriteLine("Find key-value pair: " + key + "=" + value);
            return new string[] { key, value };
        }

        bool isException(string path)
        {
            for (int i = 0; i < exceptions.Length; i++)
            {
                if (Path.GetFullPath(exceptions[i]) == path)
                {
                    Log.WriteLine("Skipping exception file!");
                    return true;
                }
            }
            return false;
        }

        string ReplaceTokens(string pageBody)
        {
            Dictionary<string, string> tokens = Config.GetInstance().GetDictionary();
            List<string> keys = new List<string>(tokens.Keys);
            for (int i = 0; i < keys.Count; i++)
            {
                string key = keys[i];
                string value = tokens[key];
                string keyToken = "{{"+key+"}}";
                string prev = pageBody;
                pageBody = pageBody.Replace(keyToken, value);
                if (prev != pageBody)
                {
                    Log.WriteLine(keyToken+" replaced to "+value);
                }
            }
            return pageBody;
        }
    }
}
