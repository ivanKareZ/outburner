using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class CommandArgumentParser{

    string[] args;

    public CommandArgumentParser(string[] args) {
        if(args == null)
            throw new Exception("Arguments cannot be null!");
        this.args = args;
    }

    public string GetString(string key, string def) {
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == key && i != args.Length - 1){
                return args[i + 1];
            }
        }
        return def;
    }

    public int GetInt(string key, int def) {
        string raw = GetString(key, def.ToString());
        int value;
        if(int.TryParse(raw, out value))
            return value;
        else
            return def;
    }

}